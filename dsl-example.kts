#!/usr/bin/env kscript

//DEPS org.jetbrains.kotlinx:kotlinx-html-jvm:0.6.12

import kotlinx.html.*
import kotlinx.html.stream.*

// Using kotlinx.html

println(createHTML().html {
    head {
        title { +"Hello there!" }
    }
    body {
        ul {
            li { +"One item" }
            li { +"Another item" }
        }
    }
})

// Using our own implementation

fun crappyHTML() = CrappyHTMLElement()

class CrappyHTMLElement(
    val tag: String = "",
    val children: MutableList<Any> = mutableListOf()
) {
    fun element(elTag: String, content: CrappyHTMLElement.() -> Unit): CrappyHTMLElement {
        val element = CrappyHTMLElement(elTag)
        element.content()
        children += element
        return this
    }

    fun html(block:  CrappyHTMLElement.() -> Unit) = element("html", block)
    fun head(block:  CrappyHTMLElement.() -> Unit) = element("head", block)
    fun title(block: CrappyHTMLElement.() -> Unit) = element("title", block)
    fun body(block:  CrappyHTMLElement.() -> Unit) = element("body", block)
    fun ul(block:    CrappyHTMLElement.() -> Unit) = element("ul", block)
    fun li(block:    CrappyHTMLElement.() -> Unit) = element("li", block)

    operator fun String.unaryPlus() { this@CrappyHTMLElement.children += this }

    override fun toString(): String {
        return if (tag.isEmpty()) {
            children.joinToString("")
        } else {
            "<$tag>${children.joinToString("")}</$tag>"
        }
    }
}

println(crappyHTML().html {
    head {
        title { +"Hello there!" }
    }
    body {
        ul {
            li { +"One item" }
            li { +"Another item" }
        }
    }
})
