//ENTRY InlineClassesExampleKt

fun main() {
    val user = User("ivo")
    val password = Password("trustno1")

    //check(password, user)
    check(user, password)
}

inline class User(val s: String)
inline class Password(val s: String)

fun check(user: User, password: Password) {
    println("Got user $user and password $password")
}

