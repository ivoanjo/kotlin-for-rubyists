package hello.micronaut

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.HttpStatus

@Controller("/rubyOnIce")
class RubyOnIceController {

    @Get("/{name}")
    fun index(name: String): Map<String, String> {
        return mapOf(
            "hello" to "nice meeting you $name"
        )
    }
}
