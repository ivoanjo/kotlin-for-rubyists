package testing.examples

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

interface User {
    fun name(): String
}

class RubyOnIce {
    fun hello(): String { return "Hello, world!" }
    fun hello(user: User): String { return "Hello there, ${user.name()}!" }
}

object RubyOnIceTest : Spek({
    val subject by memoized { RubyOnIce() }

    describe("hello") {
        it("greets everyone") {
            assertThat(subject.hello()).isEqualTo("Hello, world!")
        }

        context("when a user is supplied") {
            val name = "Ivo"
            val user = mock<User> { on { name() } doReturn(name) }

            it("greets only the supplied user") {
                println("Test println from inside testcase")
                assertThat(subject.hello(user)).isEqualTo("Hello there, Ivo!")
            }
        }
    }
})
