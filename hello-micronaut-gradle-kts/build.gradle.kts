import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    id("io.spring.dependency-management") version "1.0.6.RELEASE"
    id("com.github.johnrengelman.shadow") version "4.0.2"
    id("org.jetbrains.kotlin.jvm") version "1.2.61"
    id("org.jetbrains.kotlin.kapt") version "1.2.61"
    id("org.jetbrains.kotlin.plugin.allopen") version "1.2.61"
    application
}

version = "0.1"
group = "hello.micronaut"

repositories {
    mavenLocal()
    mavenCentral()
}

dependencyManagement {
    imports {
        mavenBom("io.micronaut:micronaut-bom:1.0.4")
    }
}

dependencies {
    compile("io.micronaut:micronaut-http-client")
    compile("io.micronaut:micronaut-http-server-netty")
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.2.61")
    compile("org.jetbrains.kotlin:kotlin-reflect:1.2.61")
    compile("io.micronaut:micronaut-runtime")
    kapt("io.micronaut:micronaut-inject-java")
    kapt("io.micronaut:micronaut-validation")
    kaptTest("io.micronaut:micronaut-inject-java")
    runtime("ch.qos.logback:logback-classic:1.2.3")
    runtime("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.7")
    testCompile("junit:junit:4.12")
    testCompile("io.micronaut:micronaut-inject-java")
    testCompile("org.hamcrest:hamcrest-all:1.3")
    testCompile("org.junit.jupiter:junit-jupiter-api:5.1.0")
    testCompile("org.jetbrains.spek:spek-api:1.1.5")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:5.1.0")
    testRuntime("org.jetbrains.spek:spek-junit-platform-engine:1.1.5")
}

application {
    mainClassName = "hello.micronaut.Application"
}

tasks.withType<Test> {
    useJUnitPlatform()
}

allOpen {
    annotation("io.micronaut.aop.Around")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
    //Will retain parameter names for Java reflection
    kotlinOptions.javaParameters = true
}
