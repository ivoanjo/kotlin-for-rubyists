if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'hello-js'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'hello-js'.");
}
this['hello-js'] = function (_, Kotlin) {
  'use strict';
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  function main(args) {
    println('Hello JavaScript!');
  }
  _.main_kand9s$ = main;
  main([]);
  Kotlin.defineModule('hello-js', _);
  return _;
}(typeof this['hello-js'] === 'undefined' ? {} : this['hello-js'], kotlin);
